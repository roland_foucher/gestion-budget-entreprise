# Gestion budget entreprise

## Context

Vous devez concevoir une application permettant aux employés d'une grande entreprise de commander du matériel.</br>
​</br>
Un salarié doit pouvoir déclencher une commande de matériel ou de logiciel. Du point de vue de l'employé, une commande comporte une catégorie, une liste d'éléments à acheter, un prix par élément, une quantité par élément, et un prix total
​</br>
Son chef de service peut valider ou refuser la commande. Si la commande est validée, un employé du service Achats procède au paiement de la commande.​</br>
​</br>
Les membres du service Comptabilité et de la Direction Générale peuvent afficher l'historique des recettes et des dépenses de l'entreprise, avec le service demandeur, et la catégorie de la dépense.
Les chefs de service peuvent consulter l'historique des dépenses de leur service.​</br>
​</br>
**Bonus**
Bonus : chaque 1er janvier, le budget de chacun des services de l'entreprise est réinitialisé.
Par exemple, le service informatique dispose d'une enveloppe annuelle de 10M€ pour gérer son fonctionnement (salaires, achats, etc). Si au 31 décembre, le service avait épuisé son budget, il reçoit 10M€ le 1er janvier. S'il restait 1M€, alors le service IT reçoit 9M€, pour disposer à nouveau de 10M€ pour l'année.
Permettez au chef de service de visualiser son budget prévisionnel, son budget restant à chaque fin de mois.​</br>
​​</br>
Bonus 2 : lors de la validation d'une commande, son prix total est déduit du budget du service. Lors du paiement par le service Achat, le montant est débité de la trésorerie de l'entreprise​</br>
​​</br>
Bonus 3 : Dans le monde professionnel, on ne paie pas forcément ses factures avant de recevoir les produits. Une facture contient généralement une date de règlement à respecter, souvent à la fin du mois.
Permettez au service Achat de valider la commande, mais de la payer à la date prévue. Les employés du service Achat peuvent consulter la liste des factures en attente de paiement.​</br>
​</br>

## Analsye des besoins

**QUOI ?**
  * obligatoire : 
    * déclencher une commande de materiel/logiciel
    * valider ou refuser la commande
    * proceder au paiement d'une commande
    * afficher un historique des recettes
    * afficher un historique des dépenses
    * consulter l'historique des dépenses d'un service


  * facultative :

**QUI ?**
  * client : entreprise commanditaire
  * utilisateur : 
  * Salariés
  * chef de service
  * employer du service d'achat 
  * membre du service comptable 
  * direction générale

**COMMENT ?**
  * context final : application web
  * techno : spring java sql thymleaf
  * délai : 3 mois a négocier avec l'entreprise

**USAGE ?**
  * usage pro d'entreprise a long terme


## useCase


![usecase](Exports/usecase.png)

## MCD

La base de donnée comprend 4 entités :

**Salarié**
  * Permet à l'utilisateur de se connecter à l'application
  * Determine le role de l'utilisateur (enums dans Java)

**Commande**
  * Définie la commade de l'utilisateur
  * Comprend un status de la commande en cour
  * Comprend un ou plusieurs articles et une catégorie

**Articles**
  * Prix et nom de l'article à commander

**Categorie**
  * Catégorie de la commande

![meriseMCDBudget](Exports/meriseMCDBudget.png)

## Diagramme de classes

**Repository**

  * chaque classe entité aura un repository avec le nom : *entité* + Repository. 
  * La classe UserRepository aura une methode getUserByName() pour récupérer le nom du user et implémenter le UserDetailService (pour gérer les accès aux différentes roots)
  * Les repository étendent JpaRepository pour utiliser JPA pour l'accès à la BDD (plus rapide à coder) 

**Services**
  * Les services ont ce nom : *utilité* + service
  * CommandeService permet de verifier le status d'une commande

**Exception**

 * Les Exceptions ont ce nom : *type d'exception* + exception
 * Il existe trois exceptions qui se déclanches en cas de mauvais comportement lié à la commande

**Controller**

  * Les Controllers ont ce nom : *utilité* + controllers
  * Les controllers renvoi une response entity soit pour renvoyer un objet au front, soit une réponse http (200,500,...)
  
![diagramme-de-classes](Exports/diagclass.png)


